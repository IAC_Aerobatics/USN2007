<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet takes care of page layout settings.
It ought to provide a default box size.
It must provide three named templates, all called from the root node.
- head-content provides content inside the head tag
- body-attributes provides attribute content for the body element
- body-content provides markup content for the body element
The third template, body-content may contaian an <apply-templates/> to
process source document elements below the root; otherwise, the stylesheet
will process no such elements.
-->
<xsl:stylesheet version='1.0' 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
 xmlns="http://www.w3.org/TR/html4/strict.dtd">

<xsl:import href="menuTemplate.xsl"/>
<xsl:import href="footerTemplate.xsl"/>

<!-- default box size for images -->
<xsl:param name="box-size">400</xsl:param>

<!-- 
  add content to the html head element.
  the calling template takes care of the title.
  link any stylesheets here.
--> 
<xsl:template name="head-content">
  <meta http-equiv="Content-Language" content="en-us" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF8" />
  <link rel="stylesheet" type="text/css" href="nationalsStyle.css" />
</xsl:template>

<xsl:template name="body-attributes"/>

<xsl:template name="body-content">
  <table>
  <tbody>
  <tr>
  <td class="headline">
  <img src="logos/NationalsLogo200.jpg" alt="Nationals Logo"/>
  </td><td class="headline">
  <h1>The 2007 United States National Aerobatic Championships</h1>
  <h2>Grayson County Airport (KGYI), Sherman/Denison, Texas</h2>
  <h2>Sunday, September 23 - Saturday, September 29, 2007</h2>
  </td>
  </tr>
  </tbody>
  </table>
  <div class="content">
    <xsl:call-template name="menu-template"/>
    <xsl:apply-templates/>
  </div>
  <xsl:call-template name="footer-template"/>
</xsl:template>

</xsl:stylesheet>
