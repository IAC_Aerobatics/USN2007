<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet:
- makes literal copy of elements within an html element.
- transalates link elements with a ref attribute into anchors with href
- translates text elements into div of class "bodyText."
- copies p elements
- translates section elements into h3 elements with class "section."
- translates subsection elements into h4 elements with class "subsection."
- translates image elements into a div block with image and caption
-->
<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns="http://www.w3.org/1999/xhtml">

<xsl:template match="html">
  <div>
    <xsl:apply-templates select="@*|node()" mode="copy"/>
  </div>
</xsl:template>

<xsl:template match="pre|img">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()" mode="copy"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="@*|node()" mode="copy">
  <xsl:copy><xsl:apply-templates select="@*|node()" mode="copy"/></xsl:copy>
</xsl:template>

<xsl:template match="p|dl|dt|dd|ol|ul|li|code|b|i|a">
  <xsl:copy>
    <xsl:apply-templates select="@*" mode="copy"/>
    <xsl:apply-templates/>
  </xsl:copy>
</xsl:template>

<xsl:template match="text">
  <div class="bodyText">
  <xsl:apply-templates/>
  </div>
</xsl:template>

<xsl:template match="section">
  <h3 class="section"><xsl:apply-templates/></h3>
</xsl:template>

<xsl:template match="subsection">
  <h4 class="subsection"><xsl:apply-templates/></h4>
</xsl:template>

<xsl:template match="image">
   <xsl:param name="image-loc">.</xsl:param>
   <xsl:param name="box-size">400</xsl:param>
   <xsl:variable name="loc">
      <xsl:choose>
        <xsl:when test="@image-loc">
           <xsl:value-of select="@image-loc"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$image-loc"/>
        </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:variable name="box">
      <xsl:choose>
        <xsl:when test="@box-size">
           <xsl:value-of select="@box-size"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$box-size"/>
        </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <div class="image-block">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="$loc"/>
          <xsl:value-of select="$file-sep"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
        <div>
        <img class="image">
           <xsl:attribute name="src">
             <xsl:value-of select="$loc"/>
             <xsl:if test = "not($box='.')">
               <xsl:value-of select="$file-sep"/>
               <xsl:value-of select="$box"/>
             </xsl:if>
             <xsl:value-of select="$file-sep"/>
             <xsl:value-of select="@name"/>
           </xsl:attribute>
           <xsl:if test="not(@caption='')">
             <xsl:attribute name="alt">
               <xsl:value-of select="@caption"/>
             </xsl:attribute>
           </xsl:if>
        </img>
        </div>
      </a>
      <div class="image-decor">
        <xsl:if test="not(@credit='')">
          <span class="image-credit" style="float:right;"><xsl:value-of select="@credit"/></span>
        </xsl:if>
        <xsl:if test="not(@caption='')">
          <span class="image-caption" style="text-align:left"><xsl:value-of select="@caption"/></span>
        </xsl:if>
      </div>
   </div>
</xsl:template>

</xsl:stylesheet>
