<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet creates index of articles with an abstract, image, and link
for each article.  Supports markup of the form:

<articleList box-size="200">
 <article
  title="August 2006 St. Hayacinthe, Quebec"
  link="2006Montreal/index.html">
  <image image-loc="2006Montreal/images" name="41770029.jpg"/>
  <abstract>
   <p>
Arrived at a beautiful little strip East of 
Montreal, St. Hyacinthe, with fields of corn, fields of wild flowers,
a man-made rectangular pond used for landing seaplanes, huge hangers with
small residences attached, and a restaurant at the end of the field.
What could be better?  Airplane heaven.
   </p>
  </abstract>
 </article>
</articleList>

The box-size attribute of articleList indicates the size of the images.
The image-loc attribute of image indicates the relative path location of
the image store.  The image store should have a subdirectory matching the 
box-size attribute.

Result markup is of form:

<table class="abstract">
 <tbody class="abstract">
  <tr class="abstract">
   <td class="abstract-tcell">
     <a href="@link">
     <h2 class="abstract-atitle">@title</h2>
     </a>
     <div class="bodyText">
      <p>content of abstract</p>
     </div>
     <p class="abstract-read-more">
      <a href="@link">Read more...</a>
     </p>
   </td>
   <td class="abstract-icell">
    <a href="@link">
     <img class="abstract-image"
      src="@image-loc/@box-size/@name"/>
    </a>
   </td>
  </tr>
 </tbody>
</table>

The image-loc attribute may appear on the articleList element or the image
element.  The value on the image element overrides the value on the articleList
element.

The link attribute of the article element is is optional.

An optional date attribute of the article element will output a dateline.
-->
<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns="http://www.w3.org/1999/xhtml">

<xsl:param name="file-sep">/</xsl:param>

<xsl:template match="articleList">
      <table class="abstract">
         <tbody class="abstract">
            <xsl:apply-templates select="article">
              <xsl:with-param name="text-side">
                <xsl:value-of select="@text-side"/>
              </xsl:with-param>
              <xsl:with-param name="box-size">
                <xsl:value-of select="@box-size"/>
              </xsl:with-param>
              <xsl:with-param name="image-loc">
                <xsl:value-of select="@image-loc"/>
              </xsl:with-param>
            </xsl:apply-templates>
         </tbody>
      </table>
</xsl:template>

<xsl:template match="article">
  <xsl:param name="text-side">right</xsl:param>
  <xsl:param name="box-size">100</xsl:param>
  <xsl:param name="image-loc">.</xsl:param>
  <tr class="abstract">
     <xsl:choose>
       <xsl:when test="$text-side='left'">
         <xsl:call-template name="abstract-text">
           <xsl:with-param name="link">
             <xsl:value-of select="@link"/>
           </xsl:with-param>
           <xsl:with-param name="title">
             <xsl:value-of select="@title"/>
           </xsl:with-param>
           <xsl:with-param name="date">
             <xsl:value-of select="@date"/>
           </xsl:with-param>
         </xsl:call-template>
         <xsl:call-template name="abstract-image">
           <xsl:with-param name="link">
             <xsl:value-of select="@link"/>
           </xsl:with-param>
           <xsl:with-param name="image-loc">
             <xsl:value-of select="$image-loc"/>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:value-of select="$box-size"/>
           </xsl:with-param>
         </xsl:call-template>
       </xsl:when>
       <xsl:otherwise>
         <xsl:call-template name="abstract-image">
           <xsl:with-param name="link">
             <xsl:value-of select="@link"/>
           </xsl:with-param>
           <xsl:with-param name="image-loc">
             <xsl:value-of select="$image-loc"/>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:value-of select="$box-size"/>
           </xsl:with-param>
         </xsl:call-template>
         <xsl:call-template name="abstract-text">
           <xsl:with-param name="link">
             <xsl:value-of select="@link"/>
           </xsl:with-param>
           <xsl:with-param name="title">
             <xsl:value-of select="@title"/>
           </xsl:with-param>
           <xsl:with-param name="date">
             <xsl:value-of select="@date"/>
           </xsl:with-param>
         </xsl:call-template>
       </xsl:otherwise>
     </xsl:choose>
  </tr>
</xsl:template>

<xsl:template name="abstract-text">
   <xsl:param name="link">.</xsl:param>
   <xsl:param name="title"/>
   <xsl:param name="date"/>
   <td class="abstract-tcell">
     <a>
     <xsl:attribute name="href"><xsl:value-of select="$link"/></xsl:attribute>
     <h2 class="abstract-atitle"><xsl:value-of select="$title"/></h2>
     <xsl:if test="$date">
       <p class="abstract-dateline"><xsl:value-of select="$date"/></p>
     </xsl:if>
     </a>
     <xsl:apply-templates select="abstract"/>
     <xsl:if test="$link">
       <p class="abstract-read-more"><a>
       <xsl:attribute name="href"><xsl:value-of select="$link"/></xsl:attribute>
       <xsl:text>Read more...</xsl:text>
       </a></p>
     </xsl:if>
   </td>
</xsl:template>

<xsl:template match="abstract">
  <div class="bodyText">
  <p><xsl:apply-templates/></p>
  </div>
</xsl:template>

<xsl:template name="abstract-image">
   <xsl:param name="image-loc">.</xsl:param>
   <xsl:param name="box-size">100</xsl:param>
   <xsl:param name="link">.</xsl:param>
   <td class="abstract-icell">
         <xsl:apply-templates select=".//image">
         <!--
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="$link"/>
        </xsl:attribute>
        <xsl:apply-templates select="image" mode="abstract-image">
        -->
           <xsl:with-param name="image-loc">
             <xsl:value-of select="$image-loc"/>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:value-of select="$box-size"/>
           </xsl:with-param>
        <!--
        </xsl:apply-templates>
      </a>
      -->
         </xsl:apply-templates>
   </td>
</xsl:template>

<xsl:template match="image" mode="abstract-image">
  <xsl:param name="image-loc">.</xsl:param>
  <xsl:param name="box-size">100</xsl:param>
  <img class="abstract-image">
     <xsl:attribute name="src">
       <xsl:choose>
       <xsl:when test="@image-loc">
           <xsl:value-of select="@image-loc"/>
       </xsl:when>
       <xsl:otherwise>
           <xsl:value-of select="$image-loc"/>
       </xsl:otherwise>
       </xsl:choose>
       <xsl:value-of select="$file-sep"/>
       <xsl:value-of select="$box-size"/>
       <xsl:value-of select="$file-sep"/>
       <xsl:value-of select="@name"/>
     </xsl:attribute>
  </img>
</xsl:template>        

</xsl:stylesheet>
