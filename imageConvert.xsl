<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet outputs a script for converting images in a photostory 
or articleList.  Assumes the convert program and resize only.
-->
<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns="http://www.w3.org/1999/xhtml">

<xsl:output 
 method="text"/>

<xsl:param name="file-sep">\</xsl:param>

<xsl:template match="text()"/>

<xsl:template match="photostory|articleList">
  <xsl:if test="@image-loc">
  <xsl:text>
mkdir </xsl:text>
  <xsl:value-of select="@image-loc"/>
  <xsl:value-of select="$file-sep"/>
  <xsl:value-of select="@box-size"/>
  </xsl:if>
  <xsl:apply-templates select=".//image">
    <xsl:with-param name="box-size">
      <xsl:value-of select="@box-size"/>
    </xsl:with-param>
    <xsl:with-param name="image-loc">
      <xsl:value-of select="@image-loc"/>
    </xsl:with-param>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="image">
  <xsl:param name="box-size">400</xsl:param>
  <xsl:param name="image-loc">./</xsl:param>
  <xsl:choose>
    <xsl:when test="@image-loc">
      <xsl:text>
mkdir </xsl:text>
      <xsl:value-of select="@image-loc"/>
      <xsl:value-of select="$file-sep"/>
      <xsl:value-of select="$box-size"/>
      <xsl:call-template name="convert">
        <xsl:with-param name="box-size">
          <xsl:value-of select="$box-size"/>
        </xsl:with-param>
        <xsl:with-param name="image-loc">
          <xsl:value-of select="@image-loc"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:call-template name="convert">
        <xsl:with-param name="box-size">
          <xsl:value-of select="$box-size"/>
        </xsl:with-param>
        <xsl:with-param name="image-loc">
          <xsl:value-of select="$image-loc"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template name="convert">
  <xsl:param name="box-size">400</xsl:param>
  <xsl:param name="image-loc">./</xsl:param>
  <xsl:text>
convert "</xsl:text>
  <xsl:value-of select="$image-loc"/>
  <xsl:value-of select="$file-sep"/>
  <xsl:value-of select="@name"/>
  <xsl:text>" -resize </xsl:text>
  <xsl:value-of select="$box-size"/>
  <xsl:text>x</xsl:text>
  <xsl:value-of select="$box-size"/>
  <xsl:text> "</xsl:text>
  <xsl:value-of select="$image-loc"/>
  <xsl:value-of select="$file-sep"/>
  <xsl:value-of select="$box-size"/>
  <xsl:value-of select="$file-sep"/>
  <xsl:value-of select="@name"/>
  <xsl:text>"</xsl:text>
</xsl:template>

</xsl:stylesheet>
