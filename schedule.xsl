<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet creates a schedule.  Supports markup of the form:

<schedule>
<schedDay date="Saturday, September 22">
<event start="8:00 AM" end="7:10 PM">Practice</event>
<event start="12:00 PM">Registration, Volunteer Sign-Up, and Technical Inspections</event>
<event>Open Evening</event>
</schedDay>
</schedule>

The date, start, and end attributes are all optional.  
schedDay elements may repeat within the schedule element.
event elements may repeat within the schedDay elements.

Result markup is of form:

<table class="schedule">
 <tbody class="schedule">
  <tr class="schedule">
   <td class="schedule-time">
   </td>
   <td class="schedule-descr">
   </td>
  </tr>
 </tbody>
</table>

-->
<xsl:stylesheet version='1.0' 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
  xmlns="http://www.w3.org/1999/xhtml">

<xsl:template match="schedule">
      <table class="schedule">
         <tbody class="schedule">
            <xsl:apply-templates/>
         </tbody>
      </table>
</xsl:template>

<xsl:template match="schedDay">
  <tr class="schedule">
    <td class="schedule-day" colspan="2"><xsl:value-of select="@date"/></td>
  </tr>
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="event">
  <tr class="schedule">
    <xsl:if test="@type">
      <xsl:attribute name="class">
        <xsl:text>schedule-</xsl:text>
        <xsl:value-of select="@type"/>
      </xsl:attribute>
    </xsl:if>
    <td class="schedule-time">
      <xsl:value-of select="@start"/>
      <xsl:if test="@end">
        <xsl:text> to </xsl:text>
        <xsl:value-of select="@end"/>
      </xsl:if>
    </td>
    <td class="schedule-descr">
      <xsl:apply-templates/>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>
