<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  xmlns="http://www.w3.org/1999/xhtml">

<xsl:template name="footer-template">
  <div class="footer">
    <p>
      Copyright ©2007 International Aerobatic Club.
      All logos, trademarks, pictures, and videos are the property 
      of their respective owners.  Contact
      <a class="footer" href="mailto:blue@wbreeze.com">Douglas Lovell</a>
      with comments or questions related to the display
      or content of this web page.
    </p>
  </div>
</xsl:template>

</xsl:stylesheet>
