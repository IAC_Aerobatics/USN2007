<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet creates a story from a photostory element.  A photostory is a series
of narratives alongside images. Supports markup of the form:

<photostory text-side="left" box-size="400" image-loc="/images/dir">
  <storyItem image-loc=".">
    <image image-loc="." name="name" caption="caption"/> (zero or more)
    ... content ...
  </storyItem>
</photostory>
  
text-side indicates whether text goes on the left or right in the story.
box-size indicates the size of the enclosing box for images.
image-loc indicates the default location for images.
The example shows defaults for these attributes.
The storyItem and image elements may override the image-loc.
The image caption is optional.  The name is not.

The result markup has form:

<table class="photostory">
 <tbody class="photostory">
  <tr class="photostory">
   <td class="photostory-tcell">
     ... content ...
   </td>
   <td class="photostory-icell">
    <div class="photostory-image-block">
     <a href="@image-loc/@name">
     <img class="photostory-image" 
      src="@image-loc/@box-size/@name" 
      alt="@caption"/>
     </a>
     <span class="photostory-image-caption">
       @caption
     </span>
    </div>
   </td>
  </tr>
 </tbody>
</table>
-->
<xsl:stylesheet version='1.0' 
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
 xmlns="http://www.w3.org/1999/xhtml">

<xsl:template match="photostory">
   <table class="photostory">
      <tbody class="photostory">
         <xsl:apply-templates>
           <xsl:with-param name="text-side">
             <xsl:choose>
               <xsl:when test="@text-side">
                 <xsl:value-of select="@text-side"/>
               </xsl:when>
               <xsl:otherwise>left</xsl:otherwise>
             </xsl:choose>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:choose>
               <xsl:when test="@box-size">
                 <xsl:value-of select="@box-size"/>
               </xsl:when>
               <xsl:otherwise>400</xsl:otherwise>
             </xsl:choose>
           </xsl:with-param>
           <xsl:with-param name="image-loc">
             <xsl:choose>
               <xsl:when test="@image-loc">
                 <xsl:value-of select="@image-loc"/>
               </xsl:when>
               <xsl:otherwise>.</xsl:otherwise>
             </xsl:choose>
           </xsl:with-param>
         </xsl:apply-templates>
      </tbody>
   </table>
</xsl:template>

<xsl:template match="storyItem">
  <xsl:param name="text-side">left</xsl:param>
  <xsl:param name="box-size">400</xsl:param>
  <xsl:param name="image-loc">.</xsl:param>
  <xsl:call-template name="photostory-row">
    <xsl:with-param name="box-size">
      <xsl:choose>
        <xsl:when test="@box-size">
           <xsl:value-of select="@box-size"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$box-size"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:with-param>
    <xsl:with-param name="text-side">
      <xsl:value-of select="$text-side"/>
    </xsl:with-param>
    <xsl:with-param name="image-loc">
      <xsl:choose>
        <xsl:when test="@image-loc">
           <xsl:value-of select="@image-loc"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$image-loc"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:with-param>
  </xsl:call-template>
</xsl:template>

<xsl:template name="photostory-row">
  <xsl:param name="image-loc">.</xsl:param>
  <xsl:param name="box-size">400</xsl:param>
  <xsl:param name="text-side">left</xsl:param>
  <tr class="photostory">
     <xsl:choose>
       <xsl:when test="$text-side='left'">
         <xsl:call-template name="photostory-text"/>
         <xsl:call-template name="photostory-image">
           <xsl:with-param name="image-loc">
             <xsl:value-of select="$image-loc"/>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:value-of select="$box-size"/>
           </xsl:with-param>
         </xsl:call-template>
       </xsl:when>
       <xsl:otherwise>
         <xsl:call-template name="photostory-image">
           <xsl:with-param name="image-loc">
             <xsl:value-of select="$image-loc"/>
           </xsl:with-param>
           <xsl:with-param name="box-size">
             <xsl:value-of select="$box-size"/>
           </xsl:with-param>
         </xsl:call-template>
         <xsl:call-template name="photostory-text"/>
       </xsl:otherwise>
     </xsl:choose>
  </tr>
</xsl:template>

<xsl:template name="photostory-text">
   <td class="photostory-tcell">
     <xsl:apply-templates select="text()|*[not(self::image)]"/>
   </td>
</xsl:template>

<xsl:template name="photostory-image">
   <xsl:param name="image-loc">.</xsl:param>
   <xsl:param name="box-size">400</xsl:param>
   <td class="photostory-icell">
     <xsl:apply-templates select="image" mode="photostory-image">
       <xsl:with-param name="box-size">
         <xsl:value-of select="$box-size"/>
       </xsl:with-param>
       <xsl:with-param name="image-loc">
         <xsl:value-of select="$image-loc"/>
       </xsl:with-param>
     </xsl:apply-templates>
   </td>
</xsl:template>

<xsl:template match="image" mode="photostory-image">
   <xsl:param name="image-loc">.</xsl:param>
   <xsl:param name="box-size">400</xsl:param>
   <xsl:variable name="loc">
      <xsl:choose>
        <xsl:when test="@image-loc">
           <xsl:value-of select="@image-loc"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$image-loc"/>
        </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <xsl:variable name="box">
      <xsl:choose>
        <xsl:when test="@box-size">
           <xsl:value-of select="@box-size"/>
        </xsl:when>
        <xsl:otherwise>
           <xsl:value-of select="$box-size"/>
        </xsl:otherwise>
      </xsl:choose>
   </xsl:variable>
   <div class="photostory-image-block">
      <a class="image-link">
        <xsl:attribute name="href">
          <xsl:value-of select="$loc"/>
          <xsl:value-of select="$file-sep"/>
          <xsl:value-of select="@name"/>
        </xsl:attribute>
        <div>
        <img class="photostory-image">
           <xsl:attribute name="src">
             <xsl:value-of select="$loc"/>
             <xsl:if test = "not($box='.')">
               <xsl:value-of select="$file-sep"/>
               <xsl:value-of select="$box"/>
             </xsl:if>
             <xsl:value-of select="$file-sep"/>
             <xsl:value-of select="@name"/>
           </xsl:attribute>
           <xsl:if test="not(@caption='')">
             <xsl:attribute name="alt">
               <xsl:value-of select="@caption"/>
             </xsl:attribute>
           </xsl:if>
        </img>
        </div>
        <xsl:if test="not(@caption='')">
          <div
           class="photostory-image-caption">
          <xsl:value-of select="@caption"/>
          </div>
        </xsl:if>
      </a>
   </div>
</xsl:template>

</xsl:stylesheet>
